<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\DocumentTrait;
use App\Mail\InvoiceMail;
use App\Mail\PasswordCustomerMail;
use App\Customer;
use App\Document;
use App\Company;
use App\Http\Requests\Api\SendEmailRequest;
use Illuminate\Support\Facades\Mail;
use Exception;

class SendEmailController extends Controller
{
    use DocumentTrait;

    /**
     * SendEmail.
     *
     *
     * @return array
     */

    public function SendEmail(SendEmailRequest $request, $GuardarEn = FALSE)
    {
        // User
        $user = auth()->user();

        // User company
        $company = $user->company;

        $document = Document::where('identification_number', '=', $company->identification_number)
                            ->where('prefix', '=', $request->prefix)
                            ->where('number', '=', $request->number)
                            ->where('state_document_id', '=', 1)->get();
        if(sizeof($document) == 0)
            return [
                'message' => "Documento {$request->prefix}-{$request->number} no existe en la base de datos.",
                'success' => FALSE,
            ];

        $customer = Customer::findOrFail($document[0]->customer);
        if($request->alternate_email)
            $email = $request->alternate_email;
        else
            $email = $customer->email;

        if($document[0]->type_document_id == 1)
            $rptafe = file_get_contents(storage_path("app/public/{$company->identification_number}/"."RptaFE-".$document[0]->prefix.$document[0]->number.".xml"));
        else
            if($document[0]->type_document_id == 4)
                $rptafe = file_get_contents(storage_path("app/public/{$company->identification_number}/"."RptaNC-".$document[0]->prefix.$document[0]->number.".xml"));
            else
                $rptafe = file_get_contents(storage_path("app/public/{$company->identification_number}/"."RptaND-".$document[0]->prefix.$document[0]->number.".xml"));
        if("b:ErrorM" !== $this->ValueXML($rptafe, "/s:Envelope/s:Body/SendBillSyncResponse/SendBillSyncResult/b:XmlFileName/"))
            $filename = str_replace('nd', 'ad', str_replace('nc', 'ad', str_replace('fv', 'ad', $this->ValueXML($rptafe, "/s:Envelope/s:Body/SendBillSyncResponse/SendBillSyncResult/b:XmlFileName/"))));
        else{
            $rptafe = file_get_contents(storage_path("app/public/{$company->identification_number}/"."RptaZIP-".$this->ValueXML($rptafe, "/s:Envelope/s:Body/SendTestSetAsyncResponse/SendTestSetAsyncResult/b:ZipKey/").".xml"));
            $filename = str_replace('nd', 'ad', str_replace('nc', 'ad', str_replace('fv', 'ad', $this->ValueXML($rptafe, "/s:Envelope/s:Body/GetStatusZipResponse/GetStatusZipResult/b:DianResponse/b:XmlFileName/"))));
        }

        if ($GuardarEn){
            if($request->base64graphicrepresentation)
                Mail::to($email)->send(new InvoiceMail($document, $customer, $company, $GuardarEn, $request->base64graphicrepresentation, $filename));
            else
                Mail::to($email)->send(new InvoiceMail($document, $customer, $company, $GuardarEn, FALSE, $filename));
        }
        else{
            if($request->base64graphicrepresentation)
                Mail::to($email)->send(new InvoiceMail($document, $customer, $company, FALSE, $request->base64graphicrepresentation, $filename));
            else
                Mail::to($email)->send(new InvoiceMail($document, $customer, $company, FALSE, FALSE, $filename));
        }
        return [
            'message' => 'Envio realizado con éxito',
            'success' => TRUE,
        ];
    }

    /**
     * SendEmail Customer.
     *
     *
     * @return view
     */

    public function SendEmailCustomer(SendEmailRequest $request, $ShowView = 'YES')
    {
        $company = Company::where('identification_number', '=', $request->company_idnumber)->first();
        if(empty($company))
            return view('customerloginmensaje', ['titulo' => 'Error al realizar el envio.',
                                                'mensaje' => 'Esta compañia no existe en la base de datos.']);

        $document = Document::where('identification_number', '=', $company->identification_number)
                            ->where('prefix', '=', $request->prefix)
                            ->where('number', '=', $request->number)
                            ->where('state_document_id', '=', 1)->get();
        if(sizeof($document) == 0)
            return view('customerloginmensaje', ['titulo' => 'Error al realizar el envio.',
                                                'mensaje' => 'Este documento no existe en la base de datos.']);

        $customer = Customer::findOrFail($document[0]->customer);

        Mail::to($customer->email)->send(new InvoiceMail($document, $customer, $company));
        if($ShowView == 'YES')
//            return redirect('/homecustomers');
            return view('customerloginmensaje', ['titulo' => 'Envio realizado con exito.',
                                                 'mensaje' => 'El Documento se envio satisfactoriamente.']);
        else
            return [
                'message' => 'Envio realizado con éxito',
                'success' => TRUE,
            ];

    }

    /**
     * SendEmail Password Customer.
     *
     *
     * @return view
     */

    public function SendEmailPasswordCustomer(Request $request)
    {
        $customer = Customer::findOrFail($request->identification_number);
        Mail::to($customer->email)->send(new PasswordCustomerMail($customer));
        return [
            'message' => 'Correo electronico para credenciales, enviado con exito.',
            'success' => 'true',
        ];
//        return view('customerloginmensaje', ['titulo' => 'Envio realizado con exito.',
//                                            'mensaje' => 'El Documento se envio satisfactoriamente.']);
    }
}
